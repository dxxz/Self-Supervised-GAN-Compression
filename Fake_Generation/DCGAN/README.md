# Deep Convolution Generative Adversarial Networks



### Experiment No.1: Distillation Experiment Results with AGP Policy of Different Sparsity Percentage ###
#### Parameters ####
    Dataset: MNIST
    Sparsity Granularity: Fine-grained
    Compression Method: Self-Supervised GAN Compression
    Final Sparsity: 25%, 50%, 75%, 90%

#### Comparative Results ####
    No.1 Column:  From Original DCGAN Generator
    No.2 Column:  From Distilled DCGAN Generator with Final Sparsity 25%
    No.3 Column:  From Distilled DCGAN Generator with Final Sparsity 50%
    No.4 Column:  From Distilled DCGAN Generator with Final Sparsity 75%
    No.5 Column:  From Distilled DCGAN Generator with Final Sparsity 90%

<center class="half">
    <img src="Results/MNIST/Baseline_Inference/generated_fake_samples_000.png" width="19.5%">
    <img src="Results/MNIST_Distill/Policy_Loss2_AGP_Inference_25p/generated_fake_samples_000.png" width="19.5%">
    <img src="Results/MNIST_Distill/Policy_Loss2_AGP_Inference_50p/generated_fake_samples_000.png" width="19.5%">
    <img src="Results/MNIST_Distill/Policy_Loss2_AGP_Inference_75p/generated_fake_samples_000.png" width="19.5%">
    <img src="Results/MNIST_Distill/Policy_Loss2_AGP_Inference_90p/generated_fake_samples_000.png" width="19.5%">
</center>

#### Typical Loss Curve of 50% Sparsity ####
<center>
    <img src="Results/MNIST_Distill/Policy_Loss2_AGP_Train_50p/DCGAN_Distilled_Loss.png" width="95%">
</center>

#### Typical Loss Curve of 75% Sparsity ####
<center>
    <img src="Results/MNIST_Distill/Policy_Loss2_AGP_Train_75p/DCGAN_Distilled_Loss.png" width="95%">
</center>


### Experiment No.2 Distillation Experiment Results with AGP Policy and Aguinaldo Method of Different Sparsity Percentage ###
#### Parameters ####
    Dataset: MNIST
    Sparsity Granularity: Fine-grained
    Compression Method: Aguinaldo Method
    Final Sparsity: 25%, 50%, 75%, 90%
#### Comparative Results ####
    No.1 Column:  From Original DCGAN Generator
    No.2 Column:  From Distilled DCGAN Generator with Aguinaldo method with Final Sparsity 25%
    No.3 Column:  From Distilled DCGAN Generator with Aguinaldo method with Final Sparsity 50%
    No.4 Column:  From Distilled DCGAN Generator with Aguinaldo method with Final Sparsity 75%
    No.5 Column:  From Distilled DCGAN Generator with Aguinaldo method with Final Sparsity 90%

<center class="half">
    <img src="Results/MNIST/Baseline_Inference/generated_fake_samples_000.png" width="19.5%">
    <img src="Results/MNIST_Distill/Aguinaldo_Loss2_Inference_25p/generated_fake_samples_000.png" width="19.5%">
    <img src="Results/MNIST_Distill/Aguinaldo_Loss2_Inference_50p/generated_fake_samples_000.png" width="19.5%">
    <img src="Results/MNIST_Distill/Aguinaldo_Loss2_Inference_75p/generated_fake_samples_000.png" width="19.5%">
    <img src="Results/MNIST_Distill/Aguinaldo_Loss2_Inference_90p/generated_fake_samples_000.png" width="19.5%">
</center>

#### Typical Loss Curve of 50% Sparsity ####
<center>
    <img src="Results/MNIST_Distill/Aguinaldo_Loss2_Train_50p/DCGAN_Distilled_Loss.png" width="95%">
</center>

#### Typical Loss Curve of 75% Sparsity ####
<center>
    <img src="Results/MNIST_Distill/Aguinaldo_Loss2_Train_75p/DCGAN_Distilled_Loss.png" width="95%">
</center>