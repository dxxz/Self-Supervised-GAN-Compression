# StarGAN

# Distillation Experiment Results #

## Overall Comparative Results: Generated Images in Inference Process ##
    No.1 Row:  From Original Pre-trained StarGAN Generator
    No.2 Row:  From Sparse Distilled StarGAN Generator with Self-Supervised Method
    No.3 Row:  From Smaller and Dense StarGAN Generator
    No.4 Row:  From Sparse Distilled StarGAN Generator with One-shot Pruning and Fine-tuning Method
    No.5 Row:  From Sparse Distilled StarGAN Generator with AGP as Fine-tuning Method
    No.6 Row:  From Sparse Distilled StarGAN Generator with AGP from Scratch Method
    No.7 Row:  From Sparse Distilled StarGAN Generator with One-shot Pruning and Distilling Method
    No.8 Row:  From Sparse Distilled StarGAN Generator with AGP During Distillation Method
    No.9 Row:  From Sparse Distilled StarGAN Generator with AGP During Distillation with Fixed Discriminator Method
    No.10 Row: From Sparse Distilled StarGAN Generator with Adversarial Learning Method
    No.11 Row: From Sparse Distilled StarGAN Generator with Knowledge Distillation Method
    No.12 Row: From Sparse Distilled StarGAN Generator with Distillation on Output of Intermediate Layers Method
    No.13 Row: From Sparse Distilled StarGAN Generator with E-M Pruning Method
    No.14 Row: From Sparse Distilled StarGAN Generator with Prune both G and D Models Method


#### Test Result of 1-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/1-images.jpg" width="99%">
</center>


#### Test Result of 101-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/101-images.jpg" width="99%">
</center>


#### Test Result of 201-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/201-images.jpg" width="99%">
</center>


#### Test Result of 301-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/301-images.jpg" width="99%">
</center>


#### Test Result of 401-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/401-images.jpg" width="99%">
</center>


#### Test Result of 501-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/501-images.jpg" width="99%">
</center>


#### Test Result of 601-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/601-images.jpg" width="99%">
</center>


#### Test Result of 701-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/701-images.jpg" width="99%">
</center>


#### Test Result of 801-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/801-images.jpg" width="99%">
</center>


#### Test Result of 901-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/901-images.jpg" width="99%">
</center>


#### Test Result of 1001-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/1001-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/1001-images.jpg" width="99%">
</center>


#### Test Result of 1101-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/1101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/1101-images.jpg" width="99%">
</center>


#### Test Result of 1201-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/1201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/1201-images.jpg" width="99%">
</center>


#### Test Result of 1301-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/1301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/1301-images.jpg" width="99%">
</center>


#### Test Result of 1401-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/1401-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/1401-images.jpg" width="99%">
</center>


#### Test Result of 1501-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/1501-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/1501-images.jpg" width="99%">
</center>


#### Test Result of 1601-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/1601-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/1601-images.jpg" width="99%">
</center>


#### Test Result of 1701-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/1701-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/1701-images.jpg" width="99%">
</center>


#### Test Result of 1801-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/1801-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/1801-images.jpg" width="99%">
</center>


#### Test Result of 1901-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/1901-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/1901-images.jpg" width="99%">
</center>


#### Test Result of 1999-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_sparsity/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Smaller_dense_baseline/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity_init/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_agp_init/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_from_scratch_sparsity/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_random_D/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_involve_D_loss/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R1/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_AAAI_D_conv_dim/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Aguinaldo_approach/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_NeurIPS_Rebuttal_R2/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_Quantization_8b/results_1/1999-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_Both_Prune/results_1/1999-images.jpg" width="99%">
</center>