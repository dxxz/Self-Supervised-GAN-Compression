# StarGAN

# Distillation Experiment Results with AGP Policy of Different Sparsity Percentage #

### Experiment Results ###
    Final Sparsity: 25%, 50%, 75%, 90%, 95%

#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  From AGP Distilled StarGAN Generator with Final Sparsity 25%
    No.2 Column:  From AGP Distilled StarGAN Generator with Final Sparsity 50%
    No.3 Column:  From AGP Distilled StarGAN Generator with Final Sparsity 75%
    No.4 Column:  From AGP Distilled StarGAN Generator with Final Sparsity 90%
    No.5 Column:  From AGP Distilled StarGAN Generator with Final Sparsity 95%

#### Test Result of 1-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128_Chong_agp_25p/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_75p/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_90p/results_1/1-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_95p/results_1/1-images.jpg" width="99%">
</center>

#### Test Result of 101-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128_Chong_agp_25p/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_75p/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_90p/results_1/101-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_95p/results_1/101-images.jpg" width="99%">
</center>

#### Test Result of 201-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128_Chong_agp_25p/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_75p/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_90p/results_1/201-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_95p/results_1/201-images.jpg" width="99%">
</center>

#### Test Result of 301-images.jpg ####
<center>
    <img src="Results/stargan_celeba_128_Chong_agp_25p/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_75p/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_90p/results_1/301-images.jpg" width="99%">
    <img src="Results/stargan_celeba_128_Chong_agp_95p/results_1/301-images.jpg" width="99%">
</center>

