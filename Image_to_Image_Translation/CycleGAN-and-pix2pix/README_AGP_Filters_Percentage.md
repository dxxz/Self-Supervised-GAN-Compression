# CycleGAN and Pix2Pix

# Distillation Experiment Results with AGP Filters Policy of Different Sparsity Percentage #

## 1. map2sat example ##
    Final Sparsity: 25%, 50%, 75%, 90%

#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  sat image from AGP Filters Distilled CycleGAN Generator B with Final Sparsity 25%
    No.2 Column:  sat image from AGP Filters Distilled CycleGAN Generator B with Final Sparsity 50%
    No.3 Column:  sat image from AGP Filters Distilled CycleGAN Generator B with Final Sparsity 75%
    No.4 Column:  sat image from AGP Filters Distilled CycleGAN Generator B with Final Sparsity 90%

#### Test Result of 1_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters/Inference/test_latest/images/1_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters_50p/Inference/test_latest/images/1_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters_75p/Inference/test_latest/images/1_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters_90p/Inference/test_latest/images/1_B_fake_B.png" width="24%">
</center>

#### Test Result of 500_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters/Inference/test_latest/images/500_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters_50p/Inference/test_latest/images/500_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters_75p/Inference/test_latest/images/500_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters_90p/Inference/test_latest/images/500_B_fake_B.png" width="24%">
</center>

#### Test Result of 1000_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters/Inference/test_latest/images/1000_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters_50p/Inference/test_latest/images/1000_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters_75p/Inference/test_latest/images/1000_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters_90p/Inference/test_latest/images/1000_B_fake_B.png" width="24%">
</center>

#### Test Result of 1098_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters/Inference/test_latest/images/1098_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters_50p/Inference/test_latest/images/1098_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters_75p/Inference/test_latest/images/1098_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_agp_filters_90p/Inference/test_latest/images/1098_B_fake_B.png" width="24%">
</center>


## 3. monet2photo example ##
    Final Sparsity: 25%, 50%, 75%, 90%

#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  photo image from AGP Filters Distilled CycleGAN Generator A with Final Sparsity 25%
    No.2 Column:  photo image from AGP Filters Distilled CycleGAN Generator A with Final Sparsity 50%
    No.3 Column:  photo image from AGP Filters Distilled CycleGAN Generator A with Final Sparsity 75%
    No.4 Column:  photo image from AGP Filters Distilled CycleGAN Generator A with Final Sparsity 90%

#### Test Result of 00010_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/00010_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/00010_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/00010_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/00010_fake_B.png" width="24%">
</center>

#### Test Result of 00110_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/00110_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/00110_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/00110_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/00110_fake_B.png" width="24%">
</center>

#### Test Result of 00210_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/00210_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/00210_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/00210_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/00210_fake_B.png" width="24%">
</center>

#### Test Result of 00310_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/00310_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/00310_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/00310_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/00310_fake_B.png" width="24%">
</center>

#### Test Result of 00410_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/00410_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/00410_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/00410_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/00410_fake_B.png" width="24%">
</center>

#### Test Result of 00610_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/00610_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/00610_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/00610_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/00610_fake_B.png" width="24%">
</center>

#### Test Result of 00710_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/00710_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/00710_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/00710_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/00710_fake_B.png" width="24%">
</center>

#### Test Result of 00810_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/00810_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/00810_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/00810_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/00810_fake_B.png" width="24%">
</center>

#### Test Result of 00910_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/00910_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/00910_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/00910_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/00910_fake_B.png" width="24%">
</center>

#### Test Result of 01010_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/01010_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/01010_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/01010_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/01010_fake_B.png" width="24%">
</center>

#### Test Result of 01110_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/01110_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/01110_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/01110_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/01110_fake_B.png" width="24%">
</center>

#### Test Result of 01210_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/01210_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/01210_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/01210_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/01210_fake_B.png" width="24%">
</center>

#### Test Result of 01310_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters/Inference/test_latest/images/01310_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_50p/Inference/test_latest/images/01310_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_75p/Inference/test_latest/images/01310_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_agp_filters_90p/Inference/test_latest/images/01310_fake_B.png" width="24%">
</center>


## 4. photo2monet example ##
    Final Sparsity: 25%, 50%, 75%, 90%

#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  monet image from AGP Filters Distilled CycleGAN Generator B with Final Sparsity 25%
    No.2 Column:  monet image from AGP Filters Distilled CycleGAN Generator B with Final Sparsity 50%
    No.3 Column:  monet image from AGP Filters Distilled CycleGAN Generator B with Final Sparsity 75%
    No.4 Column:  monet image from AGP Filters Distilled CycleGAN Generator B with Final Sparsity 90%

#### Test Result of 2014-08-01_17_41_55_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2014-08-01_17_41_55_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2014-08-01_17_41_55_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2014-08-01_17_41_55_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2014-08-01_17_41_55_fake_B.png" width="24%">
</center>

#### Test Result of 2014-08-03_09_47_19_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2014-08-03_09_47_19_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2014-08-03_09_47_19_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2014-08-03_09_47_19_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2014-08-03_09_47_19_fake_B.png" width="24%">
</center>

#### Test Result of 2014-08-04_20_20_12_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2014-08-04_20_20_12_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2014-08-04_20_20_12_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2014-08-04_20_20_12_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2014-08-04_20_20_12_fake_B.png" width="24%">
</center>

#### Test Result of 2014-08-05_21_54_30_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2014-08-05_21_54_30_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2014-08-05_21_54_30_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2014-08-05_21_54_30_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2014-08-05_21_54_30_fake_B.png" width="24%">
</center>

#### Test Result of 2014-08-07_10_27_51_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2014-08-07_10_27_51_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2014-08-07_10_27_51_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2014-08-07_10_27_51_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2014-08-07_10_27_51_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-09_11_45_42_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2015-04-09_11_45_42_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2015-04-09_11_45_42_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2015-04-09_11_45_42_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2015-04-09_11_45_42_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-15_09_50_56_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2015-04-15_09_50_56_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2015-04-15_09_50_56_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2015-04-15_09_50_56_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2015-04-15_09_50_56_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-16_16_06_04_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2015-04-16_16_06_04_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2015-04-16_16_06_04_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2015-04-16_16_06_04_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2015-04-16_16_06_04_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-19_23_42_47_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2015-04-19_23_42_47_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2015-04-19_23_42_47_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2015-04-19_23_42_47_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2015-04-19_23_42_47_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-23_19_56_22_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2015-04-23_19_56_22_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2015-04-23_19_56_22_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2015-04-23_19_56_22_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2015-04-23_19_56_22_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-24_11_19_29_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2015-04-24_11_19_29_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2015-04-24_11_19_29_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2015-04-24_11_19_29_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2015-04-24_11_19_29_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-28_20_55_45_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2015-04-28_20_55_45_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2015-04-28_20_55_45_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2015-04-28_20_55_45_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2015-04-28_20_55_45_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-29_09_09_15_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters/Inference/test_latest/images/2015-04-29_09_09_15_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_50p/Inference/test_latest/images/2015-04-29_09_09_15_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_75p/Inference/test_latest/images/2015-04-29_09_09_15_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_agp_filters_90p/Inference/test_latest/images/2015-04-29_09_09_15_fake_B.png" width="24%">
</center>


## 9. horse2zebra example ##
    Final Sparsity: 25%, 50%, 75%, 90%

#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  zebra image from AGP Filters Distilled CycleGAN Generator A with Final Sparsity 25%
    No.2 Column:  zebra image from AGP Filters Distilled CycleGAN Generator A with Final Sparsity 50%
    No.3 Column:  zebra image from AGP Filters Distilled CycleGAN Generator A with Final Sparsity 75%
    No.4 Column:  zebra image from AGP Filters Distilled CycleGAN Generator A with Final Sparsity 90%

#### Test Result of n02381460_20_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_25p/Inference/test_latest/images/n02381460_20_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_50p/Inference/test_latest/images/n02381460_20_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_75p/Inference/test_latest/images/n02381460_20_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_90p/Inference/test_latest/images/n02381460_20_fake_B.png" width="24%">
</center>

#### Test Result of n02381460_510_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_25p/Inference/test_latest/images/n02381460_510_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_50p/Inference/test_latest/images/n02381460_510_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_75p/Inference/test_latest/images/n02381460_510_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_90p/Inference/test_latest/images/n02381460_510_fake_B.png" width="24%">
</center>

#### Test Result of n02381460_3330_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_25p/Inference/test_latest/images/n02381460_3330_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_50p/Inference/test_latest/images/n02381460_3330_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_75p/Inference/test_latest/images/n02381460_3330_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_90p/Inference/test_latest/images/n02381460_3330_fake_B.png" width="24%">
</center>

#### Test Result of n02381460_1300_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_25p/Inference/test_latest/images/n02381460_1300_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_50p/Inference/test_latest/images/n02381460_1300_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_75p/Inference/test_latest/images/n02381460_1300_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_90p/Inference/test_latest/images/n02381460_1300_fake_B.png" width="24%">
</center>

#### Test Result of n02381460_1920_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_25p/Inference/test_latest/images/n02381460_1920_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_50p/Inference/test_latest/images/n02381460_1920_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_75p/Inference/test_latest/images/n02381460_1920_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_90p/Inference/test_latest/images/n02381460_1920_fake_B.png" width="24%">
</center>

#### Test Result of n02381460_9260_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_25p/Inference/test_latest/images/n02381460_9260_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_50p/Inference/test_latest/images/n02381460_9260_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_75p/Inference/test_latest/images/n02381460_9260_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_agp_filters_90p/Inference/test_latest/images/n02381460_9260_fake_B.png" width="24%">
</center>


## 10. zebra2horse example ##
    Final Sparsity: 25%, 50%, 75%, 90%

#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  horse image from AGP Filters Distilled CycleGAN Generator B with Final Sparsity 25%
    No.2 Column:  horse image from AGP Filters Distilled CycleGAN Generator B with Final Sparsity 50%
    No.3 Column:  horse image from AGP Filters Distilled CycleGAN Generator B with Final Sparsity 75%
    No.4 Column:  horse image from AGP Filters Distilled CycleGAN Generator B with Final Sparsity 90%

#### Test Result of n02391049_80_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_25p/Inference/test_latest/images/n02391049_80_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_50p/Inference/test_latest/images/n02391049_80_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_75p/Inference/test_latest/images/n02391049_80_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_90p/Inference/test_latest/images/n02391049_80_fake_B.png" width="24%">
</center>

#### Test Result of n02391049_260_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_25p/Inference/test_latest/images/n02391049_260_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_50p/Inference/test_latest/images/n02391049_260_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_75p/Inference/test_latest/images/n02391049_260_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_90p/Inference/test_latest/images/n02391049_260_fake_B.png" width="24%">
</center>

#### Test Result of n02391049_1300_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_25p/Inference/test_latest/images/n02391049_1300_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_50p/Inference/test_latest/images/n02391049_1300_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_75p/Inference/test_latest/images/n02391049_1300_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_90p/Inference/test_latest/images/n02391049_1300_fake_B.png" width="24%">
</center>

#### Test Result of n02391049_1790_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_25p/Inference/test_latest/images/n02391049_1790_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_50p/Inference/test_latest/images/n02391049_1790_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_75p/Inference/test_latest/images/n02391049_1790_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_90p/Inference/test_latest/images/n02391049_1790_fake_B.png" width="24%">
</center>

#### Test Result of n02391049_2800_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_25p/Inference/test_latest/images/n02391049_2800_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_50p/Inference/test_latest/images/n02391049_2800_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_75p/Inference/test_latest/images/n02391049_2800_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_90p/Inference/test_latest/images/n02391049_2800_fake_B.png" width="24%">
</center>

#### Test Result of n02391049_3840_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_25p/Inference/test_latest/images/n02391049_3840_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_50p/Inference/test_latest/images/n02391049_3840_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_75p/Inference/test_latest/images/n02391049_3840_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_90p/Inference/test_latest/images/n02391049_3840_fake_B.png" width="24%">
</center>

#### Test Result of n02391049_9960_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_25p/Inference/test_latest/images/n02391049_9960_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_50p/Inference/test_latest/images/n02391049_9960_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_75p/Inference/test_latest/images/n02391049_9960_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_agp_filters_90p/Inference/test_latest/images/n02391049_9960_fake_B.png" width="24%">
</center>