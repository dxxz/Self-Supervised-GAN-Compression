# CycleGAN and Pix2Pix

# Distillation Experiment Results #

## 1. map2sat example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of map
    No.2 Column:  sat image from Pre-trained CycleGAN Generator B
    No.3 Column:  sat image from Own Trained CycleGAN Generator B
    No.4 Column:  sat image from Distilled CycleGAN Generator B

#### Test Result of 1_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_map2sat_cyclegan_distill_sparsity/Inference/test_latest/images/1_B_real_A.png" width="24%">
    <img src="checkpoints/map2sat_pretrained/test_latest/images/1_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan/test_latest/images/1_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_distill_sparsity/Inference/test_latest/images/1_B_fake_B.png" width="24%">
</center>

#### Test Result of 500_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_map2sat_cyclegan_distill_sparsity/Inference/test_latest/images/500_B_real_A.png" width="24%">
    <img src="checkpoints/map2sat_pretrained/test_latest/images/500_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan/test_latest/images/500_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_distill_sparsity/Inference/test_latest/images/500_B_fake_B.png" width="24%">
</center>

#### Test Result of 1000_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_map2sat_cyclegan_distill_sparsity/Inference/test_latest/images/1000_B_real_A.png" width="24%">
    <img src="checkpoints/map2sat_pretrained/test_latest/images/1000_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan/test_latest/images/1000_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_distill_sparsity/Inference/test_latest/images/1000_B_fake_B.png" width="24%">
</center>

#### Test Result of 1098_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_map2sat_cyclegan_distill_sparsity/Inference/test_latest/images/1098_B_real_A.png" width="24%">
    <img src="checkpoints/map2sat_pretrained/test_latest/images/1098_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan/test_latest/images/1098_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_map2sat_cyclegan_distill_sparsity/Inference/test_latest/images/1098_B_fake_B.png" width="24%">
</center>


## 2. sat2map example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of sat
    No.2 Column:  map image from Pre-trained CycleGAN Generator A
    No.3 Column:  map image from Own Trained CycleGAN Generator A
    No.4 Column:  map image from Distilled CycleGAN Generator A

#### Test Result of 1_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_sat2map_cyclegan_distill_sparsity/Inference/test_latest/images/1_A_real_A.png" width="24%">
    <img src="checkpoints/sat2map_pretrained/test_latest/images/1_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_sat2map_cyclegan/test_latest/images/1_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_sat2map_cyclegan_distill_sparsity/Inference/test_latest/images/1_A_fake_B.png" width="24%">
</center>

#### Test Result of 500_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_sat2map_cyclegan_distill_sparsity/Inference/test_latest/images/500_A_real_A.png" width="24%">
    <img src="checkpoints/sat2map_pretrained/test_latest/images/500_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_sat2map_cyclegan/test_latest/images/500_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_sat2map_cyclegan_distill_sparsity/Inference/test_latest/images/500_A_fake_B.png" width="24%">
</center>

#### Test Result of 1000_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_sat2map_cyclegan_distill_sparsity/Inference/test_latest/images/1000_A_real_A.png" width="24%">
    <img src="checkpoints/sat2map_pretrained/test_latest/images/1000_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_sat2map_cyclegan/test_latest/images/1000_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_sat2map_cyclegan_distill_sparsity/Inference/test_latest/images/1000_A_fake_B.png" width="24%">
</center>

#### Test Result of 1098_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_sat2map_cyclegan_distill_sparsity/Inference/test_latest/images/1098_A_real_A.png" width="24%">
    <img src="checkpoints/sat2map_pretrained/test_latest/images/1098_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_sat2map_cyclegan/test_latest/images/1098_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_sat2map_cyclegan_distill_sparsity/Inference/test_latest/images/1098_A_fake_B.png" width="24%">
</center>


## 3. monet2photo example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of monet
    No.2 Column:  photo image from Pre-trained CycleGAN Generator A
    No.3 Column:  photo image from Own Trained CycleGAN Generator A
    No.4 Column:  photo image from Distilled CycleGAN Generator A

#### Test Result of 00010_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00010_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/00010_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/00010_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00010_fake_B.png" width="24%">
</center>

#### Test Result of 00110_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00110_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/00110_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/00110_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00110_fake_B.png" width="24%">
</center>

#### Test Result of 00210_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00210_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/00210_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/00210_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00210_fake_B.png" width="24%">
</center>

#### Test Result of 00310_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00310_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/00310_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/00310_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00310_fake_B.png" width="24%">
</center>

#### Test Result of 00410_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00410_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/00410_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/00410_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00410_fake_B.png" width="24%">
</center>

#### Test Result of 00610_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00610_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/00610_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/00610_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00610_fake_B.png" width="24%">
</center>

#### Test Result of 00710_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00710_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/00710_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/00710_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00710_fake_B.png" width="24%">
</center>

#### Test Result of 00810_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00810_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/00810_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/00810_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00810_fake_B.png" width="24%">
</center>

#### Test Result of 00910_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00910_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/00910_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/00910_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/00910_fake_B.png" width="24%">
</center>

#### Test Result of 01010_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/01010_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/01010_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/01010_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/01010_fake_B.png" width="24%">
</center>

#### Test Result of 01110_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/01110_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/01110_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/01110_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/01110_fake_B.png" width="24%">
</center>

#### Test Result of 01210_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/01210_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/01210_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/01210_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/01210_fake_B.png" width="24%">
</center>

#### Test Result of 01310_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/01310_real_A.png" width="24%">
    <img src="checkpoints/monet2photo_pretrained/test_latest/images/01310_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan/test_latest/images/01310_fake_B.png" width="24%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_distill_sparsity/Inference/test_latest/images/01310_fake_B.png" width="24%">
</center>


## 4. photo2monet example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of photo
    No.2 Column:  monet image from Pre-trained CycleGAN Generator B
    No.3 Column:  monet image from Own Trained CycleGAN Generator B
    No.4 Column:  monet image from Distilled CycleGAN Generator B

#### Test Result of 2014-08-01_17_41_55_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2014-08-01_17_41_55_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2014-08-01_17_41_55_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2014-08-01_17_41_55_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2014-08-01_17_41_55_fake_B.png" width="24%">
</center>

#### Test Result of 2014-08-03_09_47_19_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2014-08-03_09_47_19_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2014-08-03_09_47_19_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2014-08-03_09_47_19_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2014-08-03_09_47_19_fake_B.png" width="24%">
</center>

#### Test Result of 2014-08-04_20_20_12_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2014-08-04_20_20_12_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2014-08-04_20_20_12_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2014-08-04_20_20_12_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2014-08-04_20_20_12_fake_B.png" width="24%">
</center>

#### Test Result of 2014-08-05_21_54_30_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2014-08-05_21_54_30_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2014-08-05_21_54_30_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2014-08-05_21_54_30_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2014-08-05_21_54_30_fake_B.png" width="24%">
</center>

#### Test Result of 2014-08-07_10_27_51_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2014-08-07_10_27_51_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2014-08-07_10_27_51_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2014-08-07_10_27_51_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2014-08-07_10_27_51_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-09_11_45_42_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-09_11_45_42_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2015-04-09_11_45_42_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2015-04-09_11_45_42_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-09_11_45_42_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-15_09_50_56_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-15_09_50_56_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2015-04-15_09_50_56_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2015-04-15_09_50_56_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-15_09_50_56_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-16_16_06_04_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-16_16_06_04_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2015-04-16_16_06_04_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2015-04-16_16_06_04_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-16_16_06_04_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-19_23_42_47_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-19_23_42_47_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2015-04-19_23_42_47_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2015-04-19_23_42_47_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-19_23_42_47_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-23_19_56_22_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-23_19_56_22_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2015-04-23_19_56_22_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2015-04-23_19_56_22_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-23_19_56_22_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-24_11_19_29_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-24_11_19_29_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2015-04-24_11_19_29_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2015-04-24_11_19_29_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-24_11_19_29_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-28_20_55_45_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-28_20_55_45_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2015-04-28_20_55_45_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2015-04-28_20_55_45_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-28_20_55_45_fake_B.png" width="24%">
</center>

#### Test Result of 2015-04-29_09_09_15_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-29_09_09_15_real_A.png" width="24%">
    <img src="checkpoints/style_monet_pretrained/test_latest/images/2015-04-29_09_09_15_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan/test_latest/images/2015-04-29_09_09_15_fake_B.png" width="24%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_distill_sparsity/Inference/test_latest/images/2015-04-29_09_09_15_fake_B.png" width="24%">
</center>


## 5. cityscapes_label2photo example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of label
    No.2 Column:  photo image from Pre-trained CycleGAN Generator B
    No.3 Column:  photo image from Own Trained CycleGAN Generator B
    No.4 Column:  photo image from Distilled CycleGAN Generator B

#### Test Result of 1_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/1_B_real_A.png" width="24%">
    <img src="checkpoints/cityscapes_label2photo_pretrained/test_latest/images/1_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan/test_latest/images/1_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/1_B_fake_B.png" width="24%">
</center>

#### Test Result of 101_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/101_B_real_A.png" width="24%">
    <img src="checkpoints/cityscapes_label2photo_pretrained/test_latest/images/101_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan/test_latest/images/101_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/101_B_fake_B.png" width="24%">
</center>

#### Test Result of 201_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/201_B_real_A.png" width="24%">
    <img src="checkpoints/cityscapes_label2photo_pretrained/test_latest/images/201_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan/test_latest/images/201_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/201_B_fake_B.png" width="24%">
</center>

#### Test Result of 301_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/301_B_real_A.png" width="24%">
    <img src="checkpoints/cityscapes_label2photo_pretrained/test_latest/images/301_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan/test_latest/images/301_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/301_B_fake_B.png" width="24%">
</center>

#### Test Result of 401_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/401_B_real_A.png" width="24%">
    <img src="checkpoints/cityscapes_label2photo_pretrained/test_latest/images/401_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan/test_latest/images/401_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/401_B_fake_B.png" width="24%">
</center>

#### Test Result of 500_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/500_B_real_A.png" width="24%">
    <img src="checkpoints/cityscapes_label2photo_pretrained/test_latest/images/500_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan/test_latest/images/500_B_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/500_B_fake_B.png" width="24%">
</center>


## 6. cityscapes_photo2label example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of photo
    No.2 Column:  label image from Pre-trained CycleGAN Generator A
    No.3 Column:  label image from Own Trained CycleGAN Generator A
    No.4 Column:  label image from Distilled CycleGAN Generator A

#### Test Result of 1_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/1_A_real_A.png" width="24%">
    <img src="checkpoints/cityscapes_photo2label_pretrained/test_latest/images/1_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan/test_latest/images/1_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/1_A_fake_B.png" width="24%">
</center>

#### Test Result of 101_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/101_A_real_A.png" width="24%">
    <img src="checkpoints/cityscapes_photo2label_pretrained/test_latest/images/101_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan/test_latest/images/101_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/101_A_fake_B.png" width="24%">
</center>

#### Test Result of 201_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/201_A_real_A.png" width="24%">
    <img src="checkpoints/cityscapes_photo2label_pretrained/test_latest/images/201_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan/test_latest/images/201_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/201_A_fake_B.png" width="24%">
</center>

#### Test Result of 301_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/301_A_real_A.png" width="24%">
    <img src="checkpoints/cityscapes_photo2label_pretrained/test_latest/images/301_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan/test_latest/images/301_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/301_A_fake_B.png" width="24%">
</center>

#### Test Result of 401_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/401_A_real_A.png" width="24%">
    <img src="checkpoints/cityscapes_photo2label_pretrained/test_latest/images/401_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan/test_latest/images/401_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/401_A_fake_B.png" width="24%">
</center>

#### Test Result of 500_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/500_A_real_A.png" width="24%">
    <img src="checkpoints/cityscapes_photo2label_pretrained/test_latest/images/500_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan/test_latest/images/500_A_fake_B.png" width="24%">
    <img src="checkpoints/Chong_cityscapes_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/500_A_fake_B.png" width="24%">
</center>


## 7. facades_label2photo example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of label
    No.2 Column:  photo image from Pre-trained CycleGAN Generator B
    No.3 Column:  photo image from Own Trained CycleGAN Generator B
    No.4 Column:  photo image from Distilled CycleGAN Generator B

#### Test Result of 1_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/1_real_A.png" width="24%">
    <img src="checkpoints/facades_label2photo_pretrained/test_latest/images/1_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan/test_latest/images/1_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/1_fake_B.png" width="24%">
</center>

#### Test Result of 20_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/20_real_A.png" width="24%">
    <img src="checkpoints/facades_label2photo_pretrained/test_latest/images/20_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan/test_latest/images/20_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/20_fake_B.png" width="24%">
</center>

#### Test Result of 40_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/40_real_A.png" width="24%">
    <img src="checkpoints/facades_label2photo_pretrained/test_latest/images/40_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan/test_latest/images/40_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/40_fake_B.png" width="24%">
</center>

#### Test Result of 60_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/60_real_A.png" width="24%">
    <img src="checkpoints/facades_label2photo_pretrained/test_latest/images/60_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan/test_latest/images/60_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/60_fake_B.png" width="24%">
</center>

#### Test Result of 80_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/80_real_A.png" width="24%">
    <img src="checkpoints/facades_label2photo_pretrained/test_latest/images/80_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan/test_latest/images/80_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/80_fake_B.png" width="24%">
</center>

#### Test Result of 100_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/100_real_A.png" width="24%">
    <img src="checkpoints/facades_label2photo_pretrained/test_latest/images/100_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan/test_latest/images/100_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_label2photo_cyclegan_distill_sparsity/Inference/test_latest/images/100_fake_B.png" width="24%">
</center>


## 8. facades_photo2label example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of photo
    No.2 Column:  label image from Pre-trained CycleGAN Generator A
    No.3 Column:  label image from Own Trained CycleGAN Generator A
    No.4 Column:  label image from Distilled CycleGAN Generator A

#### Test Result of 1_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/1_real_A.png" width="24%">
    <img src="checkpoints/facades_photo2label_pretrained/test_latest/images/1_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan/test_latest/images/1_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/1_fake_B.png" width="24%">
</center>

#### Test Result of 20_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/20_real_A.png" width="24%">
    <img src="checkpoints/facades_photo2label_pretrained/test_latest/images/20_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan/test_latest/images/20_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/20_fake_B.png" width="24%">
</center>

#### Test Result of 40_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/40_real_A.png" width="24%">
    <img src="checkpoints/facades_photo2label_pretrained/test_latest/images/40_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan/test_latest/images/40_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/40_fake_B.png" width="24%">
</center>

#### Test Result of 60_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/60_real_A.png" width="24%">
    <img src="checkpoints/facades_photo2label_pretrained/test_latest/images/60_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan/test_latest/images/60_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/60_fake_B.png" width="24%">
</center>

#### Test Result of 80_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/80_real_A.png" width="24%">
    <img src="checkpoints/facades_photo2label_pretrained/test_latest/images/80_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan/test_latest/images/80_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/80_fake_B.png" width="24%">
</center>

#### Test Result of 100_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/100_real_A.png" width="24%">
    <img src="checkpoints/facades_photo2label_pretrained/test_latest/images/100_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan/test_latest/images/100_fake_B.png" width="24%">
    <img src="checkpoints/Chong_facades_photo2label_cyclegan_distill_sparsity/Inference/test_latest/images/100_fake_B.png" width="24%">
</center>


## 9. horse2zebra example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of horse
    No.2 Column:  zebra image from Pre-trained CycleGAN Generator A
    No.3 Column:  zebra image from Own Trained CycleGAN Generator A
    No.4 Column:  zebra image from Distilled CycleGAN Generator A

#### Test Result of n02381460_20_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_distill_sparsity/Inference/test_latest/images/n02381460_20_real_A.png" width="24%">
    <img src="checkpoints/horse2zebra_pretrained/test_latest/images/n02381460_20_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan/test_latest/images/n02381460_20_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_distill_sparsity/Inference/test_latest/images/n02381460_20_fake_B.png" width="24%">
</center>

#### Test Result of n02381460_510_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_distill_sparsity/Inference/test_latest/images/n02381460_510_real_A.png" width="24%">
    <img src="checkpoints/horse2zebra_pretrained/test_latest/images/n02381460_510_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan/test_latest/images/n02381460_510_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_distill_sparsity/Inference/test_latest/images/n02381460_510_fake_B.png" width="24%">
</center>

#### Test Result of n02381460_3330_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_distill_sparsity/Inference/test_latest/images/n02381460_3330_real_A.png" width="24%">
    <img src="checkpoints/horse2zebra_pretrained/test_latest/images/n02381460_3330_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan/test_latest/images/n02381460_3330_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_distill_sparsity/Inference/test_latest/images/n02381460_3330_fake_B.png" width="24%">
</center>

#### Test Result of n02381460_1300_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_distill_sparsity/Inference/test_latest/images/n02381460_1300_real_A.png" width="24%">
    <img src="checkpoints/horse2zebra_pretrained/test_latest/images/n02381460_1300_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan/test_latest/images/n02381460_1300_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_distill_sparsity/Inference/test_latest/images/n02381460_1300_fake_B.png" width="24%">
</center>

#### Test Result of n02381460_1920_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_distill_sparsity/Inference/test_latest/images/n02381460_1920_real_A.png" width="24%">
    <img src="checkpoints/horse2zebra_pretrained/test_latest/images/n02381460_1920_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan/test_latest/images/n02381460_1920_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_distill_sparsity/Inference/test_latest/images/n02381460_1920_fake_B.png" width="24%">
</center>

#### Test Result of n02381460_9260_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_distill_sparsity/Inference/test_latest/images/n02381460_9260_real_A.png" width="24%">
    <img src="checkpoints/horse2zebra_pretrained/test_latest/images/n02381460_9260_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan/test_latest/images/n02381460_9260_fake_B.png" width="24%">
    <img src="checkpoints/Chong_horse2zebra_cyclegan_distill_sparsity/Inference/test_latest/images/n02381460_9260_fake_B.png" width="24%">
</center>


## 10. zebra2horse example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of zebra
    No.2 Column:  horse image from Pre-trained CycleGAN Generator B
    No.3 Column:  horse image from Own Trained CycleGAN Generator B
    No.4 Column:  horse image from Distilled CycleGAN Generator B

#### Test Result of n02391049_80_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_80_real_A.png" width="24%">
    <img src="checkpoints/zebra2horse_pretrained/test_latest/images/n02391049_80_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan/test_latest/images/n02391049_80_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_80_fake_B.png" width="24%">
</center>

#### Test Result of n02391049_260_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_260_real_A.png" width="24%">
    <img src="checkpoints/zebra2horse_pretrained/test_latest/images/n02391049_260_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan/test_latest/images/n02391049_260_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_260_fake_B.png" width="24%">
</center>

#### Test Result of n02391049_1300_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_1300_real_A.png" width="24%">
    <img src="checkpoints/zebra2horse_pretrained/test_latest/images/n02391049_1300_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan/test_latest/images/n02391049_1300_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_1300_fake_B.png" width="24%">
</center>

#### Test Result of n02391049_1790_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_1790_real_A.png" width="24%">
    <img src="checkpoints/zebra2horse_pretrained/test_latest/images/n02391049_1790_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan/test_latest/images/n02391049_1790_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_1790_fake_B.png" width="24%">
</center>

#### Test Result of n02391049_2800_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_2800_real_A.png" width="24%">
    <img src="checkpoints/zebra2horse_pretrained/test_latest/images/n02391049_2800_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan/test_latest/images/n02391049_2800_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_2800_fake_B.png" width="24%">
</center>

#### Test Result of n02391049_3840_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_3840_real_A.png" width="24%">
    <img src="checkpoints/zebra2horse_pretrained/test_latest/images/n02391049_3840_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan/test_latest/images/n02391049_3840_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_3840_fake_B.png" width="24%">
</center>

#### Test Result of n02391049_9960_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_9960_real_A.png" width="24%">
    <img src="checkpoints/zebra2horse_pretrained/test_latest/images/n02391049_9960_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan/test_latest/images/n02391049_9960_fake_B.png" width="24%">
    <img src="checkpoints/Chong_zebra2horse_cyclegan_distill_sparsity/Inference/test_latest/images/n02391049_9960_fake_B.png" width="24%">
</center>