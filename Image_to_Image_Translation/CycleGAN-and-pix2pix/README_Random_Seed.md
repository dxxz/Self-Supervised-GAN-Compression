# CycleGAN and Pix2Pix

# Baseline Training With Different Random Seeds #

## 1. map2sat example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of map
    No.2 Column:  sat image from Own Trained CycleGAN Generator B with Random Seed 15
    No.3 Column:  sat image from Own Trained CycleGAN Generator B with Random Seed 63

#### Test Result of 1_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_map2sat_cyclegan_seed_15/Inference/test_latest/images/1_B_real_A.png" width="33%">
    <img src="checkpoints/Chong_map2sat_cyclegan_seed_15/Inference/test_latest/images/1_B_fake_B.png" width="33%">
    <img src="checkpoints/Chong_map2sat_cyclegan_seed_63/Inference/test_latest/images/1_B_fake_B.png" width="33%">
</center>

#### Test Result of 500_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_map2sat_cyclegan_seed_15/Inference/test_latest/images/500_B_real_A.png" width="33%">
    <img src="checkpoints/Chong_map2sat_cyclegan_seed_15/Inference/test_latest/images/500_B_fake_B.png" width="33%">
    <img src="checkpoints/Chong_map2sat_cyclegan_seed_63/Inference/test_latest/images/500_B_fake_B.png" width="33%">
</center>

#### Test Result of 1000_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_map2sat_cyclegan_seed_15/Inference/test_latest/images/1000_B_real_A.png" width="33%">
    <img src="checkpoints/Chong_map2sat_cyclegan_seed_15/Inference/test_latest/images/1000_B_fake_B.png" width="33%">
    <img src="checkpoints/Chong_map2sat_cyclegan_seed_63/Inference/test_latest/images/1000_B_fake_B.png" width="33%">
</center>

#### Test Result of 1098_B_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_map2sat_cyclegan_seed_15/Inference/test_latest/images/1098_B_real_A.png" width="33%">
    <img src="checkpoints/Chong_map2sat_cyclegan_seed_15/Inference/test_latest/images/1098_B_fake_B.png" width="33%">
    <img src="checkpoints/Chong_map2sat_cyclegan_seed_63/Inference/test_latest/images/1098_B_fake_B.png" width="33%">
</center>


## 2. sat2map example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of sat
    No.2 Column:  map image from Own Trained CycleGAN Generator A with Random Seed 15
    No.3 Column:  map image from Own Trained CycleGAN Generator A with Random Seed 63

#### Test Result of 1_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_sat2map_cyclegan_seed_15/Inference/test_latest/images/1_A_real_A.png" width="33%">
    <img src="checkpoints/Chong_sat2map_cyclegan_seed_15/Inference/test_latest/images/1_A_fake_B.png" width="33%">
    <img src="checkpoints/Chong_sat2map_cyclegan_seed_63/Inference/test_latest/images/1_A_fake_B.png" width="33%">
</center>

#### Test Result of 500_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_sat2map_cyclegan_seed_15/Inference/test_latest/images/500_A_real_A.png" width="33%">
    <img src="checkpoints/Chong_sat2map_cyclegan_seed_15/Inference/test_latest/images/500_A_fake_B.png" width="33%">
    <img src="checkpoints/Chong_sat2map_cyclegan_seed_63/Inference/test_latest/images/500_A_fake_B.png" width="33%">
</center>

#### Test Result of 1000_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_sat2map_cyclegan_seed_15/Inference/test_latest/images/1000_A_real_A.png" width="33%">
    <img src="checkpoints/Chong_sat2map_cyclegan_seed_15/Inference/test_latest/images/1000_A_fake_B.png" width="33%">
    <img src="checkpoints/Chong_sat2map_cyclegan_seed_63/Inference/test_latest/images/1000_A_fake_B.png" width="33%">
</center>

#### Test Result of 1098_A_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_sat2map_cyclegan_seed_15/Inference/test_latest/images/1098_A_real_A.png" width="33%">
    <img src="checkpoints/Chong_sat2map_cyclegan_seed_15/Inference/test_latest/images/1098_A_fake_B.png" width="33%">
    <img src="checkpoints/Chong_sat2map_cyclegan_seed_63/Inference/test_latest/images/1098_A_fake_B.png" width="33%">
</center>


## 3. monet2photo example ##
#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of monet
    No.2 Column:  photo image from Own Trained CycleGAN Generator A with Random Seed 15
    No.3 Column:  photo image from Own Trained CycleGAN Generator A with Random Seed 63

#### Test Result of 00010_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00010_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00010_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/00010_fake_B.png" width="33%">
</center>

#### Test Result of 00110_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00110_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00110_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/00110_fake_B.png" width="33%">
</center>

#### Test Result of 00210_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00210_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00210_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/00210_fake_B.png" width="33%">
</center>

#### Test Result of 00310_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00310_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00310_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/00310_fake_B.png" width="33%">
</center>

#### Test Result of 00410_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00410_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00410_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/00410_fake_B.png" width="33%">
</center>

#### Test Result of 00610_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00610_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00610_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/00610_fake_B.png" width="33%">
</center>

#### Test Result of 00710_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00710_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00710_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/00710_fake_B.png" width="33%">
</center>

#### Test Result of 00810_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00810_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00810_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/00810_fake_B.png" width="33%">
</center>

#### Test Result of 00910_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00910_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/00910_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/00910_fake_B.png" width="33%">
</center>

#### Test Result of 01010_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/01010_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/01010_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/01010_fake_B.png" width="33%">
</center>

#### Test Result of 01110_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/01110_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/01110_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/01110_fake_B.png" width="33%">
</center>

#### Test Result of 01210_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/01210_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/01210_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/01210_fake_B.png" width="33%">
</center>

#### Test Result of 01310_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/01310_real_A.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_15/Inference/test_latest/images/01310_fake_B.png" width="33%">
    <img src="checkpoints/Chong_monet2photo_cyclegan_seed_63/Inference/test_latest/images/01310_fake_B.png" width="33%">
</center>


## 4. photo2monet example ##
### Experiment No.1 ###
#### Policy ####
    Random Seed: 15
#### Training Command: Same as monet2photo ####

#### Inference Command: ####
    python test.py --dataroot ./datasets/monet2photo/testB_valid --name Chong_photo2monet_cyclegan_seed_15_Inference --model test --no_dropout --num_test 751 | tee Test_photo2monet_seed_15.log

### Experiment No.2 ###
#### Policy ####
    Random Seed: 63
#### Training Command: Same as monet2photo ####

#### Inference Command: ####
    python test.py --dataroot ./datasets/monet2photo/testB_valid --name Chong_photo2monet_cyclegan_seed_63_Inference --model test --no_dropout --num_test 751 | tee Test_photo2monet_seed_63.log

#### Comparative Results: Generated Images in Inference Process ####
    No.1 Column:  original source image of photo
    No.2 Column:  monet image from Own Trained CycleGAN Generator B with Random Seed 15
    No.3 Column:  monet image from Own Trained CycleGAN Generator B with Random Seed 63

#### Test Result of 2014-08-01_17_41_55_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2014-08-01_17_41_55_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2014-08-01_17_41_55_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2014-08-01_17_41_55_fake_B.png" width="33%">
</center>

#### Test Result of 2014-08-03_09_47_19_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2014-08-03_09_47_19_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2014-08-03_09_47_19_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2014-08-03_09_47_19_fake_B.png" width="33%">
</center>

#### Test Result of 2014-08-04_20_20_12_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2014-08-04_20_20_12_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2014-08-04_20_20_12_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2014-08-04_20_20_12_fake_B.png" width="33%">
</center>

#### Test Result of 2014-08-05_21_54_30_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2014-08-05_21_54_30_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2014-08-05_21_54_30_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2014-08-05_21_54_30_fake_B.png" width="33%">
</center>

#### Test Result of 2014-08-07_10_27_51_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2014-08-07_10_27_51_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2014-08-07_10_27_51_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2014-08-07_10_27_51_fake_B.png" width="33%">
</center>

#### Test Result of 2015-04-09_11_45_42_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-09_11_45_42_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-09_11_45_42_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2015-04-09_11_45_42_fake_B.png" width="33%">
</center>

#### Test Result of 2015-04-15_09_50_56_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-15_09_50_56_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-15_09_50_56_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2015-04-15_09_50_56_fake_B.png" width="33%">
</center>

#### Test Result of 2015-04-16_16_06_04_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-16_16_06_04_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-16_16_06_04_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2015-04-16_16_06_04_fake_B.png" width="33%">
</center>

#### Test Result of 2015-04-19_23_42_47_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-19_23_42_47_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-19_23_42_47_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2015-04-19_23_42_47_fake_B.png" width="33%">
</center>

#### Test Result of 2015-04-23_19_56_22_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-23_19_56_22_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-23_19_56_22_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2015-04-23_19_56_22_fake_B.png" width="33%">
</center>

#### Test Result of 2015-04-24_11_19_29_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-24_11_19_29_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-24_11_19_29_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2015-04-24_11_19_29_fake_B.png" width="33%">
</center>

#### Test Result of 2015-04-28_20_55_45_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-28_20_55_45_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-28_20_55_45_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2015-04-28_20_55_45_fake_B.png" width="33%">
</center>

#### Test Result of 2015-04-29_09_09_15_real_A.png ####
<center class="half">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-29_09_09_15_real_A.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_15/Inference/test_latest/images/2015-04-29_09_09_15_fake_B.png" width="33%">
    <img src="checkpoints/Chong_photo2monet_cyclegan_seed_63/Inference/test_latest/images/2015-04-29_09_09_15_fake_B.png" width="33%">
</center>
