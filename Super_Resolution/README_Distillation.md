# SRGAN

# Distillation Experiment Results #


## 1.1. Train with AGP Filters (25%, 50%) ##
### Comparative Results: ###
    Left:   Super-resolution image is generated with pre-trained dense model
    Middle: Super-resolution image is generated with AGP Filters 25% trained model
    Right:  Super-resolution image is generated with AGP Filters 50% trained model

#### Test Result of Set5: baby.png ####
<center>
    <img src="results/SRGAN_On-the-fly/set5/baby_SRGAN.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_Filters_25p/set5/baby_SRGAN_distilled.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_Filters_50p/set5/baby_SRGAN_distilled.png" width="33%">
</center>

#### Test Result of Set5: butterfly.png ####
<center>
    <img src="results/SRGAN_On-the-fly/set5/butterfly_SRGAN.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_Filters_25p/set5/butterfly_SRGAN_distilled.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_Filters_50p/set5/butterfly_SRGAN_distilled.png" width="33%">
</center>

#### Test Result of Set14: baboon.png ####
<center>
    <img src="results/SRGAN_On-the-fly/set14/baboon_SRGAN.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_Filters_25p/set14/baboon_SRGAN_distilled.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_Filters_50p/set14/baboon_SRGAN_distilled.png" width="33%">
</center>

#### Test Result of Set14: comic.png ####
<center>
    <img src="results/SRGAN_On-the-fly/set14/comic_SRGAN.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_Filters_25p/set14/comic_SRGAN_distilled.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_Filters_50p/set14/comic_SRGAN_distilled.png" width="33%">
</center>

#### Test Result of Set14: face.png ####
<center>
    <img src="results/SRGAN_On-the-fly/set14/face_SRGAN.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_Filters_25p/set14/face_SRGAN_distilled.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_Filters_50p/set14/face_SRGAN_distilled.png" width="33%">
</center>

#### Test Result of DIV2K_valid_HR: 0801.png ####
<center>
    <img src="results/SRGAN_On-the-fly/DIV2K_valid_HR/0801_SRGAN.png" width="90%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_Filters_25p/DIV2K_valid_HR/0801_SRGAN_distilled.png" width="90%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_Filters_50p/DIV2K_valid_HR/0801_SRGAN_distilled.png" width="90%">
</center>


## 1.2. Train with AGP (50%, 90%) ##
### Comparative Results: ###
    Left:   Super-resolution image is generated with pre-trained dense model
    Middle: Super-resolution image is generated with AGP 50% trained model
    Right:  Super-resolution image is generated with AGP 90% trained model

#### Test Result of Set5: baby.png ####
<center>
    <img src="results/SRGAN_On-the-fly/set5/baby_SRGAN.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp/set5/baby_SRGAN_distilled.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_90p/set5/baby_SRGAN_distilled.png" width="33%">
</center>

#### Test Result of Set5: butterfly.png ####
<center>
    <img src="results/SRGAN_On-the-fly/set5/butterfly_SRGAN.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp/set5/butterfly_SRGAN_distilled.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_90p/set5/butterfly_SRGAN_distilled.png" width="33%">
</center>

#### Test Result of Set14: baboon.png ####
<center>
    <img src="results/SRGAN_On-the-fly/set14/baboon_SRGAN.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp/set14/baboon_SRGAN_distilled.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_90p/set14/baboon_SRGAN_distilled.png" width="33%">
</center>

#### Test Result of Set14: comic.png ####
<center>
    <img src="results/SRGAN_On-the-fly/set14/comic_SRGAN.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp/set14/comic_SRGAN_distilled.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_90p/set14/comic_SRGAN_distilled.png" width="33%">
</center>

#### Test Result of Set14: face.png ####
<center>
    <img src="results/SRGAN_On-the-fly/set14/face_SRGAN.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp/set14/face_SRGAN_distilled.png" width="33%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_90p/set14/face_SRGAN_distilled.png" width="33%">
</center>

#### Test Result of DIV2K_valid_HR: 0801.png ####
<center>
    <img src="results/SRGAN_On-the-fly/DIV2K_valid_HR/0801_SRGAN.png" width="90%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp/DIV2K_valid_HR/0801_SRGAN_distilled.png" width="90%">
    <img src="results/SRGAN_On-the-fly_Distillation_Schedule_Agp_90p/DIV2K_valid_HR/0801_SRGAN_distilled.png" width="90%">
</center>