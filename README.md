# Self-Supervised-GAN-Compression

In this repo, we develop a self-supervised GAN compression technique that can be easily applied to new tasks and models, and enable meaningful comparisons between different compression granularities.


## Image Synthesis ##
We choose the DCGAN (Deep Convolution Generative Adversarial Networks) as the example for image synthesis task.

Reference paper: [Unsupervised Representation Learning with Deep Convolutional Generative Adversarial Networks](http://arxiv.org/abs/1511.06434)

The baseline implementation: [DCGAN Repository](https://github.com/soumith/dcgan.torch)

DCGAN compression results refer to [DCGAN Results](Fake_Generation/DCGAN/README.md "DCGAN Results")


## Domain Translation ##
We choose the Pix2Pix (Image-to-Image Translation with Conditional Adversarial Networks) as the example for domain translation task.

Reference paper: [Image-to-Image Translation with Conditional Adversarial Networks](https://arxiv.org/abs/1611.07004)

The baseline implementation: [Pix2Pix Repository](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix)

Pix2Pix compression results refer to [Pix2Pix Results](Image_to_Image_Translation/CycleGAN-and-pix2pix/README_Distillation.md "Pix2Pix Results"), [Fine-grained Pix2Pix Results](Image_to_Image_Translation/CycleGAN-and-pix2pix/README_AGP_Percentage.md "Fine-grained Pix2Pix Results"), [Filter-pruned Pix2Pix Results](Image_to_Image_Translation/CycleGAN-and-pix2pix/README_AGP_Filters_Percentage.md "Filter-pruned Pix2Pix Results")


## Style Transfer ##
We choose the CycleGAN (Unpaired Image-to-Image Translation using Cycle-Consistent Adversarial Networks) as the example for style transfer task.

Reference paper: [Unpaired Image-to-Image Translation using Cycle-Consistent Adversarial Networks](https://arxiv.org/abs/1703.10593)

The baseline implementation: [CycleGAN Repository](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix)

CycleGAN compression results refer to [CycleGAN Results](Image_to_Image_Translation/CycleGAN-and-pix2pix/README_Distillation.md "CycleGAN Results"), [Fine-grained CycleGAN Results](Image_to_Image_Translation/CycleGAN-and-pix2pix/README_AGP_Percentage.md "Fine-grained CycleGAN Results"), [Filter-pruned CycleGAN Results](Image_to_Image_Translation/CycleGAN-and-pix2pix/README_AGP_Filters_Percentage.md "Filter-pruned CycleGAN Results")


## Image-to-image Translation ##
We choose the StarGAN (Unified Generative Adversarial Networks for Multi-Domain Image-to-Image Translation) as the example for image-to-image translation task.

Reference paper: [Unified Generative Adversarial Networks for Multi-Domain Image-to-Image Translation](https://arxiv.org/abs/1711.09020)

The baseline implementation: [StarGAN Repository](https://github.com/yunjey/StarGAN)

StarGAN compression results refer to [StarGAN Results](Image_to_Image_Translation/StarGAN/README_Distillation.md "StarGAN Results"), [Fine-grained StarGAN Results](Image_to_Image_Translation/StarGAN/README_AGP_Percentage.md "Fine-grained StarGAN Results"), [Filter-pruned StarGAN Results](Image_to_Image_Translation/StarGAN/README_AGP_Filters_Percentage.md "Filter-pruned StarGAN Results")


## Super Resolution ##
We choose the SRGAN (Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network) as the example for super resolution task.

Reference paper: [Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network](https://arxiv.org/abs/1609.04802)

The baseline implementation: [SRGAN Repository](https://github.com/xinntao/BasicSR)

SRGAN compression results refer to [SRGAN Results](Super_Resolution/README_Distillation.md "SRGAN Results")